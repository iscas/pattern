package observer.mouse;

public enum MouseEventType {
	ON_CLICK,
	ON_DOUBLE_CLICK,
}
