package observer.mouse;

import observer.core.Event;

/**
 * 回调响应的逻辑
 * @author Administrator
 *
 */
public class MouseEventCallback {
	public void onClick(Event e){
		System.out.println("======触发单机鼠标======");
	}
}
