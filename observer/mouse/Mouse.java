package observer.mouse;

import java.lang.reflect.Method;

import observer.core.Event;
import observer.core.EventLisener;

public class Mouse extends EventLisener{
	
	public void click(){
		System.out.println("单击");
		this.trigger(MouseEventType.ON_CLICK);
	}
	
	public static void main(String[] args) throws Exception {
		//人为调用鼠标的单击事件
		Mouse mouse = new Mouse();
		MouseEventCallback callback = new MouseEventCallback();
		Method onClick = MouseEventCallback.class.getMethod("onClick", Event.class);
		
		mouse.addListener(MouseEventType.ON_CLICK, callback, onClick);
		mouse.click();
	}
}
