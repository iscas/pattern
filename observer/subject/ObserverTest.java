package observer.subject;

import java.lang.reflect.Method;

import observer.core.Event;

public class ObserverTest {

	public static void main(String[] args) {
		
		try {
			//�۲���
			Observer observer = new Observer();
	//		Observer.class.getMethod(name:"advice", new Class<?>[]{Event.class});
			Method advice;
			
			advice = Observer.class.getMethod("advice", new Class<?>[]{Event.class});
			
			//����дlily
			Subject subject = new Subject();
			subject.addListener(SubjectEventType.ON_ADD, observer, advice);
			subject.add();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
}
