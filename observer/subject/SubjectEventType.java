package observer.subject;

public enum SubjectEventType {
	ON_ADD,
	ON_REMOVE
}
