package observer.subject;

import java.lang.reflect.Method;

import observer.core.Event;
import observer.core.EventLisener;

public class Subject extends EventLisener{

	
	//通常的话，采用动态里来实现监控，避免了代码侵入
	public void add(){
		System.out.println("调用add的方法");
		trigger(SubjectEventType.ON_ADD);
	}
	
	public void remove(){
		trigger(SubjectEventType.ON_REMOVE);
		System.out.println("调用remove的方法");
	}

}
