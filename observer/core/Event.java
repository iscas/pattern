package observer.core;

import java.lang.reflect.Method;

public class Event {
	
	//事件源
	private Object source;
	//通知目标
	private Object target;
	//回调
	private Method callback;
	//触发
	private Object trigger;
	
	private long time;

	public Event(Object target, Method callback) {
		super();
		this.target = target;
		this.callback = callback;
	}

	public Object getSource() {
		return source;
	}

	public Event setSource(Object source) {
		this.source = source;
		return this;
	}

	public Object getTarget() {
		return target;
	}

	public Event setTarget(Object target) {
		this.target = target;
		return this;
	}

	public Method getCallback() {
		return callback;
	}

	public Event setCallback(Method callback) {
		this.callback = callback;
		return this;
	}

	public Object getTrigger() {
		return trigger;
	}

	public Event setTrigger(Object trigger) {
		this.trigger = trigger;
		return this;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Event [source=" + source + ", target=" + target + ", callback=" + callback + ", trigger=" + trigger
				+ ", time=" + time + "]";
	}
	
	
	
	
}
