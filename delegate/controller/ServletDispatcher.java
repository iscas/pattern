package delegate.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 相当于项目经理角色
 */
public class ServletDispatcher {
	
	private List<Handler> handlerMapping = new ArrayList<Handler>();
	
	public ServletDispatcher(){
		try {
			Class<?> memberActionClass = MemberAction.class;
			handlerMapping.add(new Handler().setController(memberActionClass.newInstance())
					.setMethod(memberActionClass.getMethod("getMemberById", new Class[]{String.class}))
					.setUrl("/web/getMemberById.json"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void doService(HttpServletRequest req, HttpServletResponse resp){
		doDispatch(req, resp);
	}
	
	public void doDispatch(HttpServletRequest req, HttpServletResponse resp){
		//1、获取用户请求的url
		//   如果按照J2EE的标准，每个url对应一个servlet，url由浏览器输入
		String uri = req.getRequestURI();
		
		//2、servlet拿到url以后，要做权衡（要做判断，要做选择）
		//   根据用户请求的url，去找到这个url对应的某一个java类的方法
		
		//3、通过拿到的url去handlerMapping（我们把它认为是策略常量
		Handler handle = null;
		for(Handler h: handlerMapping){
			if(uri.equals(h.getUrl())){
				handle = h;
				break;
			}
		}
		
		//4、将具体的任务分发给Method(通过反射去掉用其对应的方法）
		Object object = null;
		try {
			object = handle.getMethod().invoke(handle.getController(),  req.getParameter("mid"));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		//5、获取到method执行的结果，通过response返回
//		resp.getWriter().write(buf);
	}

	class Handler{
		private Object controller;
		private Method method;
		private String url;
		public Object getController() {
			return controller;
		}
		public Handler setController(Object controller) {
			this.controller = controller;
			return this;
		}
		public Method getMethod() {
			return method;
		}
		public Handler setMethod(Method method) {
			this.method = method;
			return this;
		}
		public String getUrl() {
			return url;
		}
		public Handler setUrl(String url) {
			this.url = url;
			return this;
		}
		
		
	}
}
